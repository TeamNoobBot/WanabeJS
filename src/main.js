const LineAPI = require('./api');
const { Message, OpType, Location } = require('../curve-thrift/line_types');
let exec = require('child_process').exec;

class LINE extends LineAPI {
    constructor() {
        super();
        this.receiverID = '';
        this.checkReader = [];
        this.spamName = []; 
        this.stateStatus = {      
  	      protect: 0,
            cancel: 0,
            kick: 0,
            sambut : 1,
            comment: 1,
        };
        this.messages;
        this.payload;
        this.stateUpload =  {
                file: '',
                name: '',
                group: '',
                sender: ''
            }
    }


    get myBot() {
        const bot = ['uc01e543dea7fe2d04b8c5141edc3a749'];
        return bot; 
    }
    
    get payload() {
        if(typeof this.messages !== 'undefined'){
            return (this.messages.text !== null) ? this.messages.text.split(' ').splice(1) : '' ;
        }
        return false;
    }    

    isAdminOrBot(param) {
        return this.myBot.includes(param);
    }

    getOprationType(operations) {
        for (let key in OpType) {
            if(operations.type == OpType[key]) {
                if(key !== 'NOTIFIED_UPDATE_PROFILE') {
                    console.info(`[* ${operations.type} ] ${key} `);
                }
            }
        }
    }

    poll(operation) {
        if(operation.type == 25 || operation.type == 26) {       	
            // console.log(operation);
            let message = new Message(operation.message);
            this.receiverID = message.to = (operation.message.to === this.myBot[0]) ? operation.message._from : operation.message.to ;
            Object.assign(message,{ ct: operation.createdTime.toString() });
            this.textMessage(message)
        }

        if(operation.type == 13 && this.stateStatus.cancel == 1) {
            this._cancel(operation.param2,operation.param1);
            
        }

        if(operation.type == 11 && !this.isAdminOrBot(operation.param2) && this.stateStatus.protect == 1) {
            this._kickMember(operation.param1,[operation.param2]);
            this.messages.to = operation.param1;
            this.qrOpenClose();
        }

        if(operation.type == 11 && this.stateStatus.protect == 1) { //bukattupQR       
            if(!this.isAdminOrBot(operation.param2)){
                this._kickMember(operation.param1,[operation.param2]);
            } 
        }
        
        if(operation.type == 19 && this.stateStatus.protect == 1) { //ada kick    
            // op1 = group nya
            // op2 = yang 'nge' kick
            // op3 = yang 'di' kick
            if(this.isAdminOrBot(operation.param3)) {
                this._invite(operation.param1,[operation.param3]);
            }
            if(!this.isAdminOrBot(operation.param2)){
                this._kickMember(operation.param1,[operation.param2]);
            } 

        }        

        if(operation.type == 55){ //ada reader
            const idx = this.checkReader.findIndex((v) => {
                if(v.group == operation.param1) {
                    return v
                }
            })
            if(this.checkReader.length < 1 || idx == -1) {
                this.checkReader.push({ group: operation.param1, users: [operation.param2], timeSeen: [operation.param3] });
            } else {
                for (var i = 0; i < this.checkReader.length; i++) {
                    if(this.checkReader[i].group == operation.param1) {
                        if(!this.checkReader[i].users.includes(operation.param2)) {
                            this.checkReader[i].users.push(operation.param2);
                            this.checkReader[i].timeSeen.push(operation.param3);
                        }
                    }
                }
            }
        }
        
        if(operation.type == 5 && this.stateStatus.comment == 1) { // diadd
let msg = new Message();
msg.to = operation.param1;
msg.text = "Thanks for add me!\n\nCreator : Agler - W4N4B5 -\nHttps://line.me/ti/p/~agatis2"

this._client.sendMessage(0,msg);
        }        

        if(operation.type == 17 && this.stateStatus.sambut == 1) { //joingroup
let msg2 = new Message();
msg2.to = operation.param1;
msg2.text = "Welcome Honey! (--__--)"

this._client.sendMessage(0,msg2);                   
		}
        
        if(operation.type == 15 && this.stateStatus.sambut == 1) { //leave grup
let tnb = new Message();
tnb.to = operation.param1;
tnb.text = "Bye Honey! (--__--)"

this._client.sendMessage(0,tnb);
        }
        
        if(operation.type == 16 && this.stateStatus.sambut == 1) { // botjoingroup   
let join = new Message();
join.to = operation.param1;
join.text = "Thanks for invite me!\n\nCreator: Agler - W4N4B5 -\nHttps://line.me/ti/p/~agatis2"

this._client.sendMessage(0,join);
        }
        
        if(operation.type == 13) { // diinvite
            if(this.isAdminOrBot(operation.param2)) {
                return this._acceptGroupInvitation(operation.param1);
            } else {
                return this._cancel(operation.param1,this.myBot);
            }
        }
        this.getOprationType(operation);
    }

    command(msg, reply) {
        if(this.messages.text !== null) {
            if(this.messages.text === msg.trim()) {
                if(typeof reply === 'function') {
                    reply();
                    return;
                }
                if(Array.isArray(reply)) {
                    reply.map((v) => {
                        this._sendMessage(this.messages, v);
                    })
                    return;
                }
                return this._sendMessage(this.messages, reply);
            }
        }
    }
    
    async getProfile() {
        let { displayName } = await this._myProfile();
        return displayName;
    }

    about() {            
        this._sendMessage(this.messages, `- - - - - - - - - - - - - - - - -
- - - [ INFO BOT ] - - -
- - - - - - - - - - - - - - - - -
- Bot Name : WanabeJS
- Creator : T.N.B_TEAM
- Version : Beta 6.9
- Status : Online
- Base On : NodeJS
- - - - - - - - - - - - - - - - -
- - - [ INFO BOT ] - - -
- - - - - - - - - - - - - - - - -`);
    }

    help() {            
        this._sendMessage(this.messages, `- - - - - - - - - - - - - - - - - - - - - -
- - - [ Help Command ] - - - 
- - - - - - - - - - - - - - - - - - - - - -
- - > .myid
- - > .status 
- - > .speed
- - > .about
- - > .creator
- - > .mention
- - > .set
- - > .recheck
- - > .clearall
- - > .pap tnb
- - > .pap agler
- - > .gift
- - > .spamx
- - - - - - - - - - - - - - - - - - - - - - -
- - - [ Admin Command ] - - -
- - - - - - - - - - - - - - - - - - - - - - -
- - > kick on/off
- - > cancel on/off
- - > sambut on/off
- - > comment on/off
- - > protect on/off
- - > .kickall
- - > .cancelall
- - - - - - - - - - - - - - - - - - - - - - 
- - - [ Team Noob Bot ] - - -
- - - - - - - - - - - - - - - - - - - - - -`);
    }
  
    async cancelMember() {
        let groupID;
        if(this.payload.length > 0) {
            let [ groups ] = await this._findGroupByName(this.payload.join(' '));
            groupID = groups.id;
        } 
        let gid = groupID || this.messages.to;
        let { listPendingInvite } = await this.searchGroup(gid);
        if(listPendingInvite.length > 0){
            this._cancel(gid,listPendingInvite);
        }
    }

    async searchGroup(gid) {
        let listPendingInvite = [];
        let thisgroup = await this._getGroups([gid]);
        if(thisgroup[0].invitee !== null) {
            listPendingInvite = thisgroup[0].invitee.map((key) => {
                return key.mid;
            });
        }
        let listMember = thisgroup[0].members.map((key) => {
            return { mid: key.mid, dn: key.displayName };
        });

        return { 
            listMember,
            listPendingInvite
        }
    }

    OnOff() {
        if(this.isAdminOrBot(this.messages._from)){
            let [ actions , status ] = this.messages.text.split(' ');
            const action = actions.toLowerCase();
            const state = status.toLowerCase() == 'on' ? 1 : 0;
            this.stateStatus[action] = state;
            this._sendMessage(this.messages,`Your Status: \n\n${JSON.stringify(this.stateStatus)}\n\nSupported by:\n- Team Noob Bot -`);
        } else {
            this._sendMessage(this.messages,`You Are Not Admin`);
        }
    }

    mention(listMember) {
        let mentionStrings = [''];
        let mid = [''];
        for (var i = 0; i < listMember.length; i++) {
            mentionStrings.push('@'+listMember[i].displayName+'\n');
            mid.push(listMember[i].mid);
        }
        let strings = mentionStrings.join('');
        let member = strings.split('@').slice(1);
        
        let tmp = 0;
        let memberStart = [];
        let mentionMember = member.map((v,k) => {
            let z = tmp += v.length + 1;
            let end = z - 1;
            memberStart.push(end);
            let mentionz = `{"S":"${(isNaN(memberStart[k - 1] + 1) ? 0 : memberStart[k - 1] + 1 ) }","E":"${end}","M":"${mid[k + 1]}"}`;
            return mentionz;
        })
        return {
            names: mentionStrings.slice(1),
            cmddata: { MENTION: `{"MENTIONEES":[${mentionMember}]}` }
        }
    }

    async leftGroupByName(name) {
        let payload = name || this.payload.join(' ');
        let gid = await this._findGroupByName(payload);
        for (let i = 0; i < gid.length; i++) {
            this._leaveGroup(gid[i].id);
        }
        return;
    }
    
    async recheck(cs,group) {
        let users;
        for (var i = 0; i < cs.length; i++) {
            if(cs[i].group == group) {
                users = cs[i].users;
            }
        }
        
        let contactMember = await this._getContacts(users);
        return contactMember.map((z) => {
                return { displayName: z.displayName, mid: z.mid };
            });
    }

    removeReaderByGroup(groupID) {
        const groupIndex = this.checkReader.findIndex(v => {
            if(v.group == groupID) {
                return v
            }
        })

        if(groupIndex != -1) {
            this.checkReader.splice(groupIndex,1);
        }
    }

    async getSpeed() {          	
        let curTime = Date.now() / 1000;
        await this._sendMessage(this.messages, '[ Speed Testing ]\nWaiting For A Second');
        const rtime = (Date.now() / 1000) - curTime;
        await this._sendMessage(this.messages, `${rtime} Second`);
        return;
    }
    
    
 
    async tagall() {
        let rec = await this._getGroup(this.messages.to);
        const mentions = await this.mention(rec.members);
        this.messages.contentMetadata = mentions.cmddata;
        await this._sendMessage(this.messages,mentions.names.join(''));
        return;
    }  
                
    async getTime() {
		let d = new Date();let xmenit = d.getMinutes().toString().split("");
		if(xmenit.length < 2){
			this._sendMessage(this.messages, "Time: "+d.getHours()+":0"+d.getMinutes());
		} else {
			this._sendMessage(this.messages, "Time: "+d.getHours()+":"+d.getMinutes());
		}
	}
		
    vn() {
        this._sendFile(this.messages,`${__dirname}/../download/${this.payload.join(' ')}.m4a`,3);
    }
       
    
    checkKernel() {
        exec('uname -a',(err, sto) => {
            if(err) {
                this._sendMessage(this.messages, err);
                return
            }
            this._sendMessage(this.messages, sto);
            return;
        });
    }

    setReader() {
        this._sendMessage(this.messages, `Setpoint... type '.recheck' for lookup !`);
        this.removeReaderByGroup(this.messages.to);
        return;
    }    
    clearall() {
        this._sendMessage(this.messages, `Reseted !`);
        this.checkReader = [];
        return
    }    

    creator() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'uc01e543dea7fe2d04b8c5141edc3a749',
            displayName: 'Naufal Agler' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
    }
    
    resetStateUpload() {
        this.stateUpload = {
            file: '',
            name: '',
            group: '',
            sender: ''
        };
    }

    prepareUpload() {
        this.stateUpload = {
            file: true,
            name: this.payload.join(' '),
            group: this.messages.to,
            sender: this.messages._from
        };
        this._sendMessage(this.messages,`select pict/video for upload ${this.stateUpload.name}`);
        return;
    }
    
    async doUpload({ id, contentType }) {
        let url = `https://obs-sg.line-apps.com/talk/m/download.nhn?oid=${id}`;
        await this._download(url,this.stateUpload.name, contentType);
        this.messages.contentType = 0;
        this._sendMessage(this.messages,`Upload ${this.stateUpload.name} success !!`);
        this.resetStateUpload()
        return;
    }

    searchLocalImage() {
        let name = this.payload.join(' ');
        let dirName = `${__dirname}/../download/${name}.jpg`;
        try {
            this._sendImage(this.messages,dirName);
        } catch (error) {
             this._sendImage(this.messages,`No Photo #${name} Uploaded `);
        }
        return ;
        
    }

    spamx() {
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);
                    this._sendMessage(this.messages, `Spam Nih Cuk ~`);                    
           return;
    }                    
    
    async joinQr() {
        const [ ticketId ] = this.payload[0].split('g/').splice(-1);
        let { id } = await this._findGroupByTicket(ticketId);
        await this._acceptGroupInvitationByTicket(id,ticketId);
        return;
    }

    async qrOpenClose() {
        let updateGroup = await this._getGroup(this.messages.to);
        updateGroup.preventJoinByTicket = true;
        if(typeof this.payload !== 'undefined') {
            let [ type ] = this.payload;
            if(type === 'open') {
                updateGroup.preventJoinByTicket = false;
                const groupUrl = await this._reissueGroupTicket(this.messages.to)
                this._sendMessage(this.messages,`- Team Noob Bot -\nLine group = line://ti/g/${groupUrl}`);
            }
        }
        await this._updateGroup(updateGroup);
        return;
    }


    spamGroup() {
        if(this.isAdminOrBot(this.messages._from) && this.payload[0] !== 'kill') {
            let s = [];
            for (let i = 0; i < this.payload[1]; i++) {
                let name = `${Math.ceil(Math.random() * 1000)}${i}`;
                this.spamName.push(name);
                this._createGroup(name,[this.payload[0]]);
            }
            return;
        } 
        for (let z = 0; z < this.spamName.length; z++) {
            this.leftGroupByName(this.spamName[z]);
        }
        return true;
    }

    checkIP() {
        exec(`wget ipinfo.io/${this.payload[0]} -qO -`,(err, res) => {
            if(err) {
                this._sendMessage(this.messages,'Error Please Install Wget');
                return 
            }
            const result = JSON.parse(res);
            if(typeof result.error == 'undefined') {
                const { org, country, loc, city, region } = result;
                try {
                    const [latitude, longitude ] = loc.split(',');
                    let location = new Location();
                    Object.assign(location,{ 
                        title: `Location:`,
                        address: `${org} ${city} [ ${region} ]\n${this.payload[0]}`,
                        latitude: latitude,
                        longitude: longitude,
                        phone: null 
                    })
                    const Obj = { 
                        text: 'Location',
                        location : location,
                        contentType: 0,
                    }
                    Object.assign(this.messages,Obj)
                    this._sendMessage(this.messages,'Location');
                } catch (err) {
                    this._sendMessage(this.messages,'Not Found');
                }
            } else {
                this._sendMessage(this.messages,'Location Not Found , Maybe di dalem goa');
            }
        })
        return;
    }

    async rechecks() {
        let rec = await this.recheck(this.checkReader,this.messages.to);
        const mentions = await this.mention(rec);
        this.messages.contentMetadata = mentions.cmddata;
        await this._sendMessage(this.messages,mentions.names.join('')+"\n\n{ Sider Tercyduk Cuk }");
        return;
    }

    async kickAll() {
        let groupID;
        if(this.stateStatus.kick == 1 && this.isAdminOrBot(this.messages._from)) {
            let target = this.messages.to;
            if(this.payload.length > 0) {
                let [ groups ] = await this._findGroupByName(this.payload.join(' '));
                groupID = groups.id;
            }
            let { listMember } = await this.searchGroup(groupID || target);
            for (var i = 0; i < listMember.length; i++) {
                if(!this.isAdminOrBot(listMember[i].mid)){
                    this._kickMember(groupID || target,[listMember[i].mid])
                }
            }
            return;
        } 
        return this._sendMessage(this.messages, ' Kick Failed check status or admin only !');
    }

    async checkIG() {
        try {
            let { userProfile, userName, bio, media, follow } = await this._searchInstagram(this.payload[0]);
            await this._sendFileByUrl(this.messages,userProfile);
            await this._sendMessage(this.messages, `${userName}\n\nBIO:\n${bio}\n\n\uDBC0 ${follow} \uDBC0`)
            if(Array.isArray(media)) {
                for (let i = 0; i < media.length; i++) {
                    await this._sendFileByUrl(this.messages,media[i]);
                }
            } else {
                this._sendMessage(this.messages,media);
            }
        } catch (error) {
            this._sendMessage(this.messages,`Error: ${error}`);
        }
        return;
    }    

    async textMessage(messages) {
        this.messages = messages;
        let payload = (this.messages.text !== null) ? this.messages.text.split(' ').splice(1).join(' ') : '' ;
        let receiver = messages.to;
        let sender = messages.from;      

        
        this.command('Halo', ['halo juga','ini siapa?']);
        this.command('kamu siapa', this.getProfile.bind(this));
        this.command('.status', `Your Status: ${JSON.stringify(this.stateStatus)}`);
        this.command(`.left ${payload}`, this.leftGroupByName.bind(this));
        this.command('.speed', this.getSpeed.bind(this)); 
        this.command('.help', this.help.bind(this));       
        this.command('help', this.help.bind(this));
        this.command('.about', this.about.bind(this));        
        this.command('.time', this.getTime.bind(this));
        this.command(`.mention`,this.tagall.bind(this));
        this.command('.kernel', this.checkKernel.bind(this));
        this.command(`kick ${payload}`, this.OnOff.bind(this));
        this.command(`protect ${payload}`, this.OnOff.bind(this));
        this.command(`comment ${payload}`, this.OnOff.bind(this));
        this.command(`cancel ${payload}`, this.OnOff.bind(this));        
        this.command(`.kickall ${payload}`,this.kickAll.bind(this));
        this.command(`.cancelall ${payload}`, this.cancelMember.bind(this));
        this.command(`.set`,this.setReader.bind(this));
        this.command(`.recheck`,this.rechecks.bind(this));            
        this.command(`.spamx ${payload}`,this.spamx.bind(this));
        this.command(`.clearall`,this.clearall.bind(this));
        this.command('.myid',`Your ID: ${messages.from}`)
        this.command(`.ip ${payload}`,this.checkIP.bind(this))
        this.command(`.ig ${payload}`,this.checkIG.bind(this))
        this.command(`.qr ${payload}`,this.qrOpenClose.bind(this))
        this.command(`.joinqr ${payload}`,this.joinQr.bind(this));
        this.command(`.spam ${payload}`,this.spamGroup.bind(this));
        this.command(`.creator`,this.creator.bind(this));

        this.command(`.pap ${payload}`,this.searchLocalImage.bind(this));        
        this.command(`.upload ${payload}`,this.prepareUpload.bind(this));
        this.command(`vn ${payload}`,this.vn.bind(this));                

  

        if(messages.contentType == 13) {
            messages.contentType = 0;
            if(!this.isAdminOrBot(messages.contentMetadata.mid)) {
                this._sendMessage(messages,messages.contentMetadata.mid);
            }
            return;
        }

        if(this.stateUpload.group == messages.to && [1,2,3].includes(messages.contentType)) {
            if(sender === this.stateUpload.sender) {
                this.doUpload(messages);
                return;
            } else {
                messages.contentType = 0;
                this._sendMessage(messages,'Wrong Sender !! Reseted');
            }
            this.resetStateUpload();
            return;
        }
        
  if (messages.text == '.gift'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'PRDID': 'a0768339-c2d3-4189-9653-2909e9bb6f58',
                                    'PRDTYPE': 'THEME',
                                    'MSGTPL': '6'},messages.contentType=9);
     }

        // if(cmd == 'lirik') {
        //     let lyrics = await this._searchLyrics(payload);
        //     this._sendMessage(seq,lyrics);
        // }

    }

}

module.exports = LINE;
